package com.tec.cley.project.database;

import com.tec.cley.project.models.Doctor;
import com.tec.cley.project.models.Patient;

import java.util.Date;
import java.util.List;

/**
 * Created by miguelchan on 31/03/17.
 */

public interface PatientRepository {

    int createPatient(Patient patient, Doctor doctor);
    int deletePatient(Patient patient);
    int updatePatient(Patient oldPatient, Patient newPatient);
    List<Patient> getPatients();
    List<Patient> getPatientsFromDate(Date startDate, Date endDate);

}
