package com.tec.cley.project.database.ormlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.tec.cley.project.models.Doctor;
import com.tec.cley.project.models.Patient;

import java.sql.SQLException;

import javax.inject.Inject;

/**
 * Created by miguelchan on 31/03/17.
 */
public class OrmliteHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = OrmliteHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "Project.db";
    private static final int DATABASE_VERSION = 1;

    private static Class[] mClasses = {
            Doctor.class,
            Patient.class
    };

    @Inject
    public OrmliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database,
                         ConnectionSource connectionSource) {
        for(Class currentClass : mClasses) {
            try {
                int updated = TableUtils.createTable(connectionSource, currentClass);
                Log.d(TAG, "Created Table: " + currentClass.getSimpleName() + " -> " + updated);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database,
                          ConnectionSource connectionSource,
                          int oldVersion,
                          int newVersion) {
        dropDatabase();
        onCreate(database, connectionSource);
    }

    public void dropDatabase() {
        for(Class currentClass : mClasses) {
            try {
                TableUtils.dropTable(getConnectionSource(), currentClass, true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
