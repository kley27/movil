package com.tec.cley.project.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.tec.cley.project.models.User;
import com.tec.cley.project.utils.AppUtils;

/**
 * Created by miguelchan on 30/03/17.
 */

public class LogInService extends IntentService {

    private static final String TAG = LogInService.class.getSimpleName();

    public static final String BROADCAST_ACTION_LOG_IN = "BALogIn";

    public static final String BROADCAST_EXTRA_LOGGED_IN = "BAExtraLoggedin";
    public static final String BROADCAST_EXTRA_USER = "BAExtraUser";

    private static final String EXTRA_USER = "ExtraUser";

    public static class Builder {

        private Intent mIntent;
        private Context mContext;

        public static Builder newInstance(Context context) {
            return new Builder(context);
        }

        public Builder setUser(User user) {
            mIntent.putExtra(EXTRA_USER, user);
            return this;
        }

        public void buildAndStart() {
            mContext.startService(mIntent);
        }

        private Builder(Context context) {
            mContext = context;
            mIntent = new Intent(context, LogInService.class);
            mIntent.setAction(BROADCAST_ACTION_LOG_IN);
        }

    }

    public LogInService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(AppUtils.isNull(intent)) {
            return;
        }

        String action = intent.getAction();
        if(BROADCAST_ACTION_LOG_IN.equals(action)) {
            startLogInProcess(intent);
        }
    }

    private void startLogInProcess(Intent intent) {
        User user = intent.getParcelableExtra(EXTRA_USER);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        intent = new Intent(BROADCAST_ACTION_LOG_IN);
        intent.putExtra(BROADCAST_EXTRA_USER, user);
        intent.putExtra(BROADCAST_EXTRA_LOGGED_IN, true);

        sendLocalBroadcast(intent);
    }

    private void sendLocalBroadcast(Intent intent) {
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}
