package com.tec.cley.project.controllers;

import com.tec.cley.project.models.Patient;

import java.util.Date;
import java.util.List;

/**
 * Created by miguelchan on 31/03/17.
 */

public interface PatientsController {

    List<Patient> getPatients();
    boolean deletePatient(Patient patient);
    List<Patient> getPatientsFromDate(Date startDate, Date endDate);

}
