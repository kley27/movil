package com.tec.cley.project.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.tec.cley.project.CustomApplication;
import com.tec.cley.project.modules.components.Injector;

/**
 * Created by miguelchan on 31/03/17.
 */

public final class InjectorUtils {

    public static Injector getInjector(Fragment fragment) {
        return getInjector(fragment.getActivity());
    }

    public static Injector getInjector(Activity activity) {
        return getInjector(activity.getApplication());
    }

    public static Injector getInjector(Context context) {
        return getInjector((Application)context.getApplicationContext());
    }

    public static Injector getInjector(Application application) {
        if(application instanceof CustomApplication) {
            return ((CustomApplication) application).getInjector();
        }
        return null;
    }

    private InjectorUtils() {
        throw new AssertionError("No Instances");
    }

}
