package com.tec.cley.project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;

import com.tec.cley.project.R;

/**
 * Created by miguelchan on 29/03/17.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash);

        if(savedInstanceState == null) {
            configureSleepThread();
        }
    }

    private void configureSleepThread() {
        Thread thread = new Thread(this::sleepAndLoadActivity);
        thread.start();
    }

    private void sleepAndLoadActivity() {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        loadLogInActivity();
    }

    private void loadLogInActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent = IntentCompat.makeRestartActivityTask(intent.getComponent());
        startActivity(intent);
    }
}
