package com.tec.cley.project.database.ormlite;

import com.j256.ormlite.dao.Dao;
import com.tec.cley.project.database.PatientRepository;
import com.tec.cley.project.models.Doctor;
import com.tec.cley.project.models.Patient;
import com.tec.cley.project.utils.AppUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by miguelchan on 31/03/17.
 */

public class OrmlitePatientRepository implements PatientRepository {

    private OrmliteHelper mHelper;
    private Dao<Patient, Integer> mDao;

    @Inject
    public OrmlitePatientRepository(OrmliteHelper ormliteHelper) {
        mHelper = ormliteHelper;
        createDao();
    }

    @Override
    public int createPatient(Patient patient, Doctor doctor) {
        if(patient.getDoctor() == null) {
            patient.setDoctor(doctor);
        }

        try {
            return mDao.create(patient);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deletePatient(Patient patient) {
        try {
            return mDao.delete(patient);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int updatePatient(Patient oldPatient, Patient newPatient) {
        try {
            return mDao.update(newPatient);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Patient> getPatients() {
        try {
            return mDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public List<Patient> getPatientsFromDate(Date startDate, Date endDate) {
        List<Patient> patientList = new ArrayList<>();
        try {
            if (AppUtils.isNull(endDate)) {
                patientList = mDao.queryBuilder()
                        .orderBy(Patient.DATE_OF_ADMISSION, true)
                        .where()
                        .ge(Patient.DATE_OF_ADMISSION, startDate)
                        .query();
            } else {
                patientList = mDao.queryBuilder()
                        .orderBy(Patient.DATE_OF_ADMISSION, true)
                        .where()
                        .between(Patient.DATE_OF_ADMISSION, startDate, endDate)
                        .query();
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        return patientList;
    }

    private void createDao() {
        try {
            mDao = mHelper.getDao(Patient.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
