package com.tec.cley.project.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.IntentCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;

import com.tec.cley.project.R;
import com.tec.cley.project.databinding.LoginLayoutBinding;
import com.tec.cley.project.models.User;
import com.tec.cley.project.services.LogInService;
import com.tec.cley.project.utils.AppUtils;

/**
 * Created by miguelchan on 29/03/17.
 */

public class LoginActivity extends AppCompatActivity {

    public static final String TAG = LoginActivity.class.getSimpleName();

    private static final String EXTRA_USER = "ExtraUser";

    private User mUser;
    private LoginLayoutBinding mBinding;

    private TextInputEditText mUserNameEditText;
    private TextInputEditText mPasswordEditText;
    private CheckBox mUserTypeCheckBox;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerForNotifications();
        mayRestoreInstanceState(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.login_layout);
        mBinding.setUser(mUser);
        configureDataBinding();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterForNotifications();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(EXTRA_USER, mUser);
    }

    private void configureDataBinding() {
        mBinding.loginLayoutLoginButton.setOnClickListener(this::onLoginClick);

        mUserNameEditText = mBinding.loginLayoutUserName;
        mPasswordEditText = mBinding.loginLayoutUserName;
        mUserTypeCheckBox = mBinding.loginLayoutIsDoctor;
    }

    private void onLoginClick(View view) {
        refreshUser();

        LogInService
                .Builder
                .newInstance(this)
                .setUser(mUser)
                .buildAndStart();
    }

    private void refreshUser() {
        String userName = mUserNameEditText.getText().toString();
        String password = mPasswordEditText.getText().toString();

        if(AppUtils.isValidString(userName) && AppUtils.isValidString(password)) {
            mUser.setUserName(userName);
            mUser.setPassword(password);

            int userType = getUserType();
            mUser.setType(userType);
        }
    }

    private int getUserType() {
        return mUserTypeCheckBox.isChecked() ? User.TYPE_DOCTOR : User.TYPE_PATIENT;
    }

    private void mayRestoreInstanceState(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            mUser = savedInstanceState.getParcelable(EXTRA_USER);
        } else {
            mUser = new User(0, "", "", User.TYPE_PATIENT);
        }
    }

    private void registerForNotifications() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(LogInService.BROADCAST_ACTION_LOG_IN);

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private void processLogInBroadcast(Intent intent) {
        boolean isLoggedIn = intent.getBooleanExtra(LogInService.BROADCAST_EXTRA_LOGGED_IN, false);
        if(isLoggedIn) {
            User user = intent.getParcelableExtra(LogInService.BROADCAST_EXTRA_USER);
            loadMainActivity(user);
        }
    }

    private void loadMainActivity(User user) {
        Intent intent = new Intent(this, MainActivity.class);
        intent = IntentCompat.makeRestartActivityTask(intent.getComponent());
        startActivity(intent);
    }

    private void unregisterForNotifications() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(LogInService.BROADCAST_ACTION_LOG_IN.equals(action)) {
                processLogInBroadcast(intent);
            }
        }
    };
}
