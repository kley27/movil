package com.tec.cley.project.modules;

import android.content.Context;

import com.tec.cley.project.controllers.ConsultationController;
import com.tec.cley.project.controllers.PatientsController;
import com.tec.cley.project.controllers.impl.BaseConsultationController;
import com.tec.cley.project.controllers.impl.BasePatientsController;
import com.tec.cley.project.database.DoctorRepository;
import com.tec.cley.project.database.PatientRepository;
import com.tec.cley.project.database.ormlite.OrmliteDoctorRepository;
import com.tec.cley.project.database.ormlite.OrmliteHelper;
import com.tec.cley.project.database.ormlite.OrmlitePatientRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by miguelchan on 31/03/17.
 */
@Module
public class AppModule {

    private Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }

    @Provides
    public Context providesContext() {
        return mContext;
    }

    @Provides
    public DoctorRepository providesDoctorRepository(OrmliteHelper ormliteHelper) {
        return new OrmliteDoctorRepository(ormliteHelper);
    }

    @Provides
    public PatientRepository providesPatientRepository(OrmliteHelper ormliteHelper) {
        return new OrmlitePatientRepository(ormliteHelper);
    }

    @Provides
    public OrmliteHelper providesOrmliteHelper(Context context) {
        return new OrmliteHelper(context);
    }

    @Provides
    public ConsultationController providesConsultationController(DoctorRepository doctorRepository,
                                                                 PatientRepository patientRepository) {
        return new BaseConsultationController(doctorRepository, patientRepository);
    }

    @Provides
    public PatientsController providesPatientsController(PatientRepository patientRepository) {
        return new BasePatientsController(patientRepository);
    }
}
