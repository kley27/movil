package com.tec.cley.project.database;

import com.tec.cley.project.models.Doctor;

/**
 * Created by miguelchan on 31/03/17.
 */

public interface DoctorRepository {

    int createDoctor(Doctor doctor);
    Doctor findDoctorById(int id);
    int updateDoctor(Doctor doctor);

}
