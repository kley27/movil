package com.tec.cley.project.utils;

import android.databinding.BaseObservable;
import android.databinding.BindingConversion;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by miguelchan on 29/03/17.
 */
public final class BindingConverters extends BaseObservable {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

    @BindingConversion
    public static String convertDateToString(Date date) {
        if(AppUtils.isNull(date)) {
            return "";
        }
        return DATE_FORMAT.format(date);
    }

    @BindingConversion
    public static String convertIntToString(int id) {
        return String.valueOf(id);
    }

}
