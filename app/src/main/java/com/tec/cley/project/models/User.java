package com.tec.cley.project.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.databinding.library.baseAdapters.BR;

/**
 * Created by miguelchan on 29/03/17.
 */
public class User extends BaseObservable implements Parcelable {

    public static final String ID = "Id";
    public static final String USERNAME = "UserName";
    public static final String PASSWORD = "Password";
    public static final String TYPE = "Type";

    public static final int TYPE_DOCTOR = 1;
    public static final int TYPE_PATIENT = 2;

    private int accountId;
    private String userName;
    private String password;
    private int type;

    public User(int accountId, String userName, String password, int type) {
        this.accountId = accountId;
        this.userName = userName;
        this.password = password;
        this.type = type;
    }

    protected User(Parcel in) {
        accountId = in.readInt();
        userName = in.readString();
        password = in.readString();
        type = in.readInt();
    }

    public void setUserName(String userName) {
        this.userName = userName;
        notifyPropertyChanged(BR.userName);
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }

    public void setType(int type) {
        this.type = type;
        notifyPropertyChanged(BR.type);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public int getAccountId() {
        return accountId;
    }

    @Bindable
    public String getUserName() {
        return userName;
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    @Bindable
    public int getType() {
        return type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(accountId);
        parcel.writeString(userName);
        parcel.writeString(password);
        parcel.writeInt(type);
    }
}
