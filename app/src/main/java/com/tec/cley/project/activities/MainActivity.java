package com.tec.cley.project.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;

import com.tec.cley.project.R;
import com.tec.cley.project.fragments.PatientListFragment;
import com.tec.cley.project.fragments.PatientsFragment;
import com.tec.cley.project.fragments.QueryFragment;
import com.tec.cley.project.utils.AppUtils;

/**
 * Created by miguelchan on 30/03/17.
 */

public class MainActivity extends AppCompatActivity {

    private NavigationView mNavigationView;
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        configureViews();
        if(savedInstanceState == null) {
            loadFirstFragment();
        }
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    public void loadFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainLayout_fragmentContainer, fragment, tag);
        if(addToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }

    private void loadFirstFragment() {
        PatientsFragment patientsFragment = PatientsFragment.newInstance();
        loadFragment(patientsFragment, PatientsFragment.TAG, false);
    }


    private void configureViews() {
        mNavigationView = AppUtils.findViewById(this, R.id.mainLayout_navigationView);
        mToolbar = AppUtils.findViewById(this, R.id.toolbar);
        mDrawerLayout = AppUtils.findViewById(this, R.id.mainLayout_drawerLayout);

        configureNavigationView();
        configureToolbar();
        configureDrawerToggle();
    }

    private void configureDrawerToggle() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, 0, 0);
    }

    private void loadRemoveFragment() {
        PatientListFragment fragment = PatientListFragment.newInstance(true);
        loadFragment(fragment, PatientListFragment.TAG, false);
    }

    private void loadEditFragment() {
        PatientListFragment fragment = PatientListFragment.newInstance(false);
        loadFragment(fragment, PatientListFragment.TAG, false);
    }

    private void configureToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void configureNavigationView() {
        mNavigationView.setNavigationItemSelectedListener(this::onNavigationItemSelected);
    }

    private void loadQueryFragment() {
        QueryFragment queryFragment = QueryFragment.newInstance();
        loadFragment(queryFragment, QueryFragment.TAG, false);
    }

    private boolean onNavigationItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if(itemId == R.id.navigationMenu_remove) {
            loadRemoveFragment();
        } else if(itemId == R.id.navigationMenu_add) {
            loadFirstFragment();
        } else if(itemId == R.id.navigationMenu_search) {
            loadQueryFragment();
        } else if(itemId == R.id.navigationMenu_update) {
            loadEditFragment();
        }
        mDrawerLayout.closeDrawer(Gravity.START);
        return true;
    }
}
