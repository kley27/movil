package com.tec.cley.project.adapters.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tec.cley.project.adapters.PatientsAdapter;
import com.tec.cley.project.databinding.RowPatientBinding;
import com.tec.cley.project.models.Patient;
import com.tec.cley.project.utils.AppUtils;

/**
 * Created by miguelchan on 31/03/17.
 */

public class PatientViewHolder extends RecyclerView.ViewHolder {

    private RowPatientBinding mBinding;
    private PatientsAdapter.OnPatientClickListener mOnPatientClickListener;

    public PatientViewHolder(View itemView,
                             PatientsAdapter.OnPatientClickListener onPatientClickListener) {
        super(itemView);
        itemView.setOnClickListener(this::onClick);
        mOnPatientClickListener = onPatientClickListener;
        mBinding = RowPatientBinding.bind(itemView);
    }

    public void configure(Patient patient) {
        mBinding.setPatient(patient);
        mBinding.setDoctor(patient.getDoctor());
    }

    private void onClick(View view) {
        if(AppUtils.isNotNull(mOnPatientClickListener)) {
            mOnPatientClickListener.onPatientClick(mBinding.getPatient(), getAdapterPosition(), view);
        }
    }

}
