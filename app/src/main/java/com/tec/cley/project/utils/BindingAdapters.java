package com.tec.cley.project.utils;

import android.databinding.BindingAdapter;
import android.support.design.widget.TextInputEditText;
import android.support.v4.util.Pair;
import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by miguelchan on 29/03/17.
 */

public class BindingAdapters {

    private static final int KEY_TEXT_WATCHER = 1999;

    @BindingAdapter({"app:binding"})
    public static void setBinding(TextInputEditText view, String string) {

    }

    /*
    @BindingAdapter({"app:binding"})
    public static void setBinding(TextInputEditText view, final BindingConverters bindableString) {
        Pair<BindingConverters, TextWatcher> pair = (Pair) view.getTag(KEY_TEXT_WATCHER);
        if (pair == null || pair.first != bindableString) {
            if (pair != null) {
                view.removeTextChangedListener(pair.second);
            }

            TextWatcher watcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    bindableString.set(s.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            };
            view.setTag(KEY_TEXT_WATCHER, new Pair<>(bindableString, watcher));
            view.addTextChangedListener(watcher);
        }
        String newValue = bindableString.get();
        if (!view.getText().toString().equals(newValue)) {
            view.setText(newValue);
        }
    }*/

}
