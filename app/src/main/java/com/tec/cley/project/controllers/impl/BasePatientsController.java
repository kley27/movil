package com.tec.cley.project.controllers.impl;

import com.tec.cley.project.controllers.PatientsController;
import com.tec.cley.project.database.PatientRepository;
import com.tec.cley.project.models.Patient;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by miguelchan on 31/03/17.
 */

public class BasePatientsController implements PatientsController {

    private PatientRepository mPatientRepository;

    @Inject
    public BasePatientsController(PatientRepository patientRepository) {
        mPatientRepository = patientRepository;
    }

    @Override
    public List<Patient> getPatients() {
        return mPatientRepository.getPatients();
    }

    @Override
    public boolean deletePatient(Patient patient) {
        return mPatientRepository.deletePatient(patient) > 0;
    }

    @Override
    public List<Patient> getPatientsFromDate(Date startDate, Date endDate) {
        return mPatientRepository.getPatientsFromDate(startDate, endDate);
    }
}
