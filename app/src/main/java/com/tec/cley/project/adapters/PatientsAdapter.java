package com.tec.cley.project.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tec.cley.project.R;
import com.tec.cley.project.adapters.holders.PatientViewHolder;
import com.tec.cley.project.models.Patient;

import java.util.List;

/**
 * Created by miguelchan on 31/03/17.
 */

public class PatientsAdapter extends RecyclerView.Adapter<PatientViewHolder> {

    private List<Patient> mPatients;
    private OnPatientClickListener mOnPatientClickListener;

    public interface OnPatientClickListener {
        void onPatientClick(Patient patient, int position, View view);
    }

    public PatientsAdapter(List<Patient> patients) {
        mPatients = patients;
    }

    @Override
    public PatientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mainView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new PatientViewHolder(mainView, mOnPatientClickListener);
    }

    public void setOnPatientClickListener(OnPatientClickListener onPatientClickListener) {
        mOnPatientClickListener = onPatientClickListener;
    }

    @Override
    public void onBindViewHolder(PatientViewHolder holder, int position) {
        Patient currentPatient = mPatients.get(position);
        holder.configure(currentPatient);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_patient;
    }

    @Override
    public int getItemCount() {
        return mPatients.size();
    }
}
