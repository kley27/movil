package com.tec.cley.project.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tec.cley.project.R;
import com.tec.cley.project.adapters.PatientsAdapter;
import com.tec.cley.project.controllers.PatientsController;
import com.tec.cley.project.databinding.LayoutFiltersBinding;
import com.tec.cley.project.models.Patient;
import com.tec.cley.project.utils.AppUtils;
import com.tec.cley.project.utils.InjectorUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by miguelchan on 31/03/17.
 */

public class QueryFragment extends Fragment {

    public static final String TAG = QueryFragment.class.getSimpleName();

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

    private ArrayList<Patient> mPatients;

    @Inject
    /*package*/ PatientsController mPatientsController;

    private LayoutFiltersBinding mBinding;
    private RecyclerView mRecyclerView;
    private TextInputEditText mStartDateEditText;
    private TextInputEditText mEndDateEditText;

    public static QueryFragment newInstance() {
        return new QueryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InjectorUtils.getInjector(this).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_filters, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = LayoutFiltersBinding.bind(view);

        mRecyclerView = mBinding.filtersLayoutRecyclerView;
        mStartDateEditText = mBinding.filtersLayoutStartDate;
        mEndDateEditText = mBinding.filtersLayoutEndDate;

        mBinding.filtersLayoutSearch.setOnClickListener(this::onSearchClick);
    }

    private void onSearchClick(View view) {
        String startDateString = mStartDateEditText.getText().toString();
        String endDateString = mEndDateEditText.getText().toString();

        if(!AppUtils.isValidString(startDateString)) {
            Toast.makeText(getActivity(), "Se requiere la fecha de inicio", Toast.LENGTH_LONG).show();
            return;
        }

        Date startDate = null;
        try {
            startDate = DATE_FORMAT.parse(startDateString);
            mStartDateEditText.setError(null);
        } catch (ParseException e) {
            mStartDateEditText.setError("Fecha Inválida");
            return;
        }

        Date endDate = null;
        if(AppUtils.isValidString(endDateString)) {
            try {
                endDate = DATE_FORMAT.parse(endDateString);
                mEndDateEditText.setError(null);
            } catch (ParseException e) {
                mEndDateEditText.setError("Fecha Inválida");
            }
        }

        mPatients = AppUtils.newArrayList(mPatientsController.getPatientsFromDate(startDate, endDate));
        loadAdapter();
    }

    private void loadAdapter() {
        if(mPatients == null) {
            mPatients = AppUtils.newArrayList(mPatientsController.getPatients());
        }

        PatientsAdapter adapter = new PatientsAdapter(mPatients);
        mRecyclerView.setAdapter(adapter);
    }
}
