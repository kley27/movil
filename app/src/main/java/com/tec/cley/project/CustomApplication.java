package com.tec.cley.project;

import android.app.Application;

import com.tec.cley.project.modules.AppModule;
import com.tec.cley.project.modules.components.DaggerInjector;
import com.tec.cley.project.modules.components.Injector;

/**
 * Created by miguelchan on 31/03/17.
 */

public class CustomApplication extends Application {

    private Injector mInjector;

    public Injector getInjector() {
        if(mInjector == null) {
            createInjector();
        }
        return mInjector;
    }

    private void createInjector() {
        mInjector = DaggerInjector
                .builder()
                .appModule(new AppModule(this))
                .build();
    }

}
