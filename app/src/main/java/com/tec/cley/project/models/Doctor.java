package com.tec.cley.project.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by miguelchan on 29/03/17.
 */
@DatabaseTable
public class Doctor implements Parcelable {

    public static final String ID = "Id";
    public static final String NAME = "Name";

    @DatabaseField(columnName = ID, id = true)
    private int id;
    @DatabaseField(columnName = NAME)
    private String name;

    public Doctor(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Doctor() {

    }

    protected Doctor(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    public static final Creator<Doctor> CREATOR = new Creator<Doctor>() {
        @Override
        public Doctor createFromParcel(Parcel in) {
            return new Doctor(in);
        }

        @Override
        public Doctor[] newArray(int size) {
            return new Doctor[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
    }
}
