package com.tec.cley.project.controllers.impl;

import com.tec.cley.project.controllers.ConsultationController;
import com.tec.cley.project.database.DoctorRepository;
import com.tec.cley.project.database.PatientRepository;
import com.tec.cley.project.models.Doctor;
import com.tec.cley.project.models.Patient;

import javax.inject.Inject;

/**
 * Created by miguelchan on 31/03/17.
 */

public class BaseConsultationController implements ConsultationController {

    private DoctorRepository mDoctorRepository;
    private PatientRepository mPatientRepository;

    @Inject
    public BaseConsultationController(DoctorRepository doctorRepository,
                                      PatientRepository patientRepository) {
        mDoctorRepository = doctorRepository;
        mPatientRepository = patientRepository;
    }

    @Override
    public boolean addConsultation(Patient patient, Doctor doctor) {
        doctor = createAndRetrieveDoctor(doctor);

        return mPatientRepository.createPatient(patient, doctor) > 0;
    }

    @Override
    public boolean editConsultation(Patient patient, Doctor doctor) {
        if(mDoctorRepository.updateDoctor(doctor) > 0) {
            return mPatientRepository.updatePatient(patient, patient) > 0;
        }
        return false;
    }

    private Doctor createAndRetrieveDoctor(Doctor doctor) {
        Doctor dbDoctor = mDoctorRepository.findDoctorById(doctor.getId());
        if(dbDoctor == null) {
            mDoctorRepository.createDoctor(doctor);
            return doctor;
        }
        return dbDoctor;
    }
}
