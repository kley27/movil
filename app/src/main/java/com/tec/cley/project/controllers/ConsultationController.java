package com.tec.cley.project.controllers;

import com.tec.cley.project.models.Doctor;
import com.tec.cley.project.models.Patient;

/**
 * Created by miguelchan on 31/03/17.
 */

public interface ConsultationController {

    boolean addConsultation(Patient patient, Doctor doctor);
    boolean editConsultation(Patient patient, Doctor doctor);

}
