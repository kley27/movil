package com.tec.cley.project.database.ormlite;

import com.j256.ormlite.dao.Dao;
import com.tec.cley.project.database.DoctorRepository;
import com.tec.cley.project.models.Doctor;

import java.sql.SQLException;

import javax.inject.Inject;

/**
 * Created by miguelchan on 31/03/17.
 */

public class OrmliteDoctorRepository implements DoctorRepository {

    private OrmliteHelper mHelper;
    private Dao<Doctor, Integer> mDoctorDao;

    @Inject
    public OrmliteDoctorRepository(OrmliteHelper helper) {
        mHelper = helper;
        createDao();
    }

    @Override
    public int createDoctor(Doctor doctor) {
        try {
            return mDoctorDao.create(doctor);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int updateDoctor(Doctor doctor) {
        try {
            return mDoctorDao.update(doctor);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Doctor findDoctorById(int id) {
        try {
            return mDoctorDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void createDao() {
        try {
            mDoctorDao = mHelper.getDao(Doctor.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
