package com.tec.cley.project.modules.components;

import com.tec.cley.project.fragments.PatientListFragment;
import com.tec.cley.project.fragments.PatientsFragment;
import com.tec.cley.project.fragments.QueryFragment;
import com.tec.cley.project.modules.AppModule;

import dagger.Component;

/**
 * Created by miguelchan on 31/03/17.
 */
@Component(modules = {
        AppModule.class
})
public interface Injector {

    void inject(PatientsFragment patientsFragment);
    void inject(PatientListFragment patientListFragment);
    void inject(QueryFragment queryFragment);

}
