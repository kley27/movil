package com.tec.cley.project.models;

import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tec.cley.project.utils.Constants;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import java.util.Date;

/**
 * Created by miguelchan on 29/03/17.
 */
@DatabaseTable
public class Patient implements Parcelable {

    public static final String NAME = "Name";
    public static final String DATE_OF_ADMISSION = "DoA";
    public static final String AGE = "Age";
    public static final String ID = "Id";
    public static final String DOCTOR_ID = "DoctorId";

    @DatabaseField(columnName = NAME)
    private String name;
    @DatabaseField(columnName = DATE_OF_ADMISSION, dataType = DataType.DATE_LONG)
    private Date dateOfAdmission;
    @DatabaseField(columnName = AGE)
    private int age;
    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = DOCTOR_ID, foreign = true, foreignColumnName = Doctor.ID)
    private Doctor doctor;

    public Patient(String name, Date dateOfAdmission,Doctor doctor,  int age, int id) {
        this.name = name;
        this.dateOfAdmission = dateOfAdmission;
        this.age = age;
        this.id = id;
        this.doctor = doctor;
    }

    public Patient() { }

    protected Patient(Parcel in) {
        name = in.readString();
        age = in.readInt();
        id = in.readInt();
        doctor = in.readParcelable(Doctor.class.getClassLoader());
    }

    public static final Creator<Patient> CREATOR = new Creator<Patient>() {
        @Override
        public Patient createFromParcel(Parcel in) {
            return new Patient(in);
        }

        @Override
        public Patient[] newArray(int size) {
            return new Patient[size];
        }
    };

    public void setName(String name) {
        this.name = name;
    }

    public String getPricePerDay() {
        LocalDate localDate = new LocalDate(dateOfAdmission);
        LocalDate endDate = new LocalDate(new Date());

        int daysBetween = Days.daysBetween(localDate, endDate).getDays();
        return String.valueOf(Constants.PRICE_PER_DAY * daysBetween) + " MXN";
    }

    public void setDateOfAdmission(Date dateOfAdmission) {
        this.dateOfAdmission = dateOfAdmission;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public String getName() {
        return name;
    }

    public Date getDateOfAdmission() {
        return dateOfAdmission;
    }

    public int getAge() {
        return age;
    }

    public int getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeInt(age);
        parcel.writeInt(id);
        parcel.writeParcelable(doctor, i);
    }
}
