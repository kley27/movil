package com.tec.cley.project.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tec.cley.project.R;
import com.tec.cley.project.controllers.ConsultationController;
import com.tec.cley.project.databinding.LayoutAddPatientBinding;
import com.tec.cley.project.models.Doctor;
import com.tec.cley.project.models.Patient;
import com.tec.cley.project.utils.AppUtils;
import com.tec.cley.project.utils.InjectorUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by miguelchan on 30/03/17.
 */

public class PatientsFragment extends Fragment {

    public static final String TAG = PatientsFragment.class.getSimpleName();

    private static final String EXTRA_PATIENT = "ExtraPatient";
    private static final String EXTRA_EDIT = "ExtraEdit";

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

    @Inject
    /*package*/ ConsultationController mConsultationController;

    private LayoutAddPatientBinding mBinding;

    private TextInputEditText mDoctorNameEditText;
    private TextInputEditText mDoctorIdEditText;

    private TextInputEditText mPatientNameEditText;
    private TextInputEditText mPatientDoAEditText;
    private TextInputEditText mPatientAgeEditText;

    private Doctor mDoctor;
    private Patient mPatient;
    private boolean mIsEdition;

    public static PatientsFragment newInstance(Patient patient) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_PATIENT, patient);
        args.putBoolean(EXTRA_EDIT, true);

        PatientsFragment patientsFragment = new PatientsFragment();
        patientsFragment.setArguments(args);

        return patientsFragment;
    }

    public static PatientsFragment newInstance() {
        return new PatientsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InjectorUtils.getInjector(this).inject(this);
        configureArguments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.layout_add_patient, container, false);
        mBinding = LayoutAddPatientBinding.bind(mainView);

        return mainView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        configureViews();
    }

    private void configureArguments() {
        Bundle args = getArguments();
        if(AppUtils.isNotNull(args)) {
            mPatient = args.getParcelable(EXTRA_PATIENT);
            mDoctor = mPatient.getDoctor();
            mIsEdition = args.getBoolean(EXTRA_EDIT);
        }
    }

    private void configureViews() {
        mDoctorIdEditText = mBinding.doctorId;
        mDoctorNameEditText = mBinding.doctorName;
        mPatientAgeEditText = mBinding.patientAge;
        mPatientDoAEditText = mBinding.patientDateOfAdmissions;
        mPatientNameEditText = mBinding.patientName;

        mBinding.createPatient.setOnClickListener(this::onCreatePatientClick);

        if(AppUtils.isNotNull(mDoctor)) {
            mBinding.setDoctor(mDoctor);
            mDoctorIdEditText.setText(String.valueOf(mDoctor.getId()));
        }

        if(AppUtils.isNotNull(mPatient)) {
            mBinding.setPatient(mPatient);
            mPatientAgeEditText.setText(String.valueOf(mPatient.getAge()));
        }

        if(mIsEdition) {
            mBinding.createPatient.setText("Editar Paciente");
        }
    }

    private void onCreatePatientClick(View view) {
        if(retrieveDoctorFromViews() && retrievePatientFromViews()) {
            if(mIsEdition) {
                editConsultation();
            } else {
                createConsultation();
            }
        }
    }

    private void editConsultation() {
        if(mConsultationController.editConsultation(mPatient, mDoctor)) {
            Toast.makeText(getActivity(), "Paciente Editado", Toast.LENGTH_LONG).show();
            getActivity().getSupportFragmentManager().popBackStack();
        } else {
            Toast.makeText(getActivity(), "Hubo un problema", Toast.LENGTH_LONG).show();
        }
    }

    private void createConsultation() {
        boolean created = mConsultationController.addConsultation(mPatient, mDoctor);
        if(created) {
            Toast.makeText(getActivity(), "Consulta Creada con Éxito", Toast.LENGTH_LONG).show();
            clearViews();
        } else {
            Toast.makeText(getActivity(), "Error al Crear la Consulta", Toast.LENGTH_LONG).show();
        }
    }

    private void clearViews() {
        mPatient = new Patient("", null, null, 0, 0);
        mDoctor = new Doctor(0, "");

        mBinding.setPatient(mPatient);
        mBinding.setDoctor(mDoctor);

        mPatientAgeEditText.setText("");
        mDoctorIdEditText.setText("");
    }

    private boolean retrievePatientFromViews() {
        String patientAgeString = mPatientAgeEditText.getText().toString();
        int patientAge = 0;

        try {
            patientAge = Integer.parseInt(patientAgeString);
            mPatientAgeEditText.setError(null);
        }catch (Exception e) {
            mPatientAgeEditText.setError("Edad Inválida");
            return false;
        }

        String patientName = mPatientNameEditText.getText().toString();
        if(!AppUtils.isValidString(patientName)) {
            mPatientNameEditText.setError("Nombre Inválido");
            return false;
        }

        mPatientNameEditText.setError(null);

        Date dateOfAdmission = null;
        String dateString = mPatientDoAEditText.getText().toString();
        try {
            dateOfAdmission = DATE_FORMAT.parse(dateString);
            mPatientDoAEditText.setError(null);
        }catch (Exception e) {
            mPatientDoAEditText.setError("Fecha de Ingreso Inválida");
            return false;
        }

        mPatient = new Patient(patientName,
                dateOfAdmission,
                mDoctor,
                patientAge,
                mPatient == null ? 0 : mPatient.getId()
        );

        return true;
    }

    private boolean retrieveDoctorFromViews() {
        String doctorIdString = mDoctorIdEditText.getText().toString();

        int doctorId = 0;
        try {
            doctorId = Integer.parseInt(doctorIdString);
            mDoctorIdEditText.setError(null);
        } catch(Exception e) {
            mDoctorIdEditText.setError("Id Inválido");
            return false;
        }

        String doctorName = mDoctorNameEditText.getText().toString();
        if(!AppUtils.isValidString(doctorName)) {
            mDoctorNameEditText.setError("Nombre Inválido");
            return false;
        }

        mDoctorNameEditText.setError(null);

        mDoctor = new Doctor(doctorId, doctorName);
        return true;
    }
}
