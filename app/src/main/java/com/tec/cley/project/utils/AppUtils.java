package com.tec.cley.project.utils;

import android.app.Activity;
import android.view.View;

import com.tec.cley.project.models.Doctor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Created by miguelchan on 29/03/17.
 */

public final class AppUtils {

    public static <T> ArrayList<T> newArrayList(Collection<T> collection) {
        return new ArrayList<T>(collection);
    }

    public static boolean isNotNull(Object object) {
        return object != null;
    }

    public static boolean isValidString(String string) {
        return string != null && string.length() > 0;
    }

    public static <T extends View> T findViewById(Activity activity, int resource) {
        return ( T ) activity.findViewById(resource);
    }

    public static boolean isNull(Object object) {
        return object == null;
    }

    public static <T> T firstNotNull(T t1, T t2) {
        return t1 == null ? t2 : t1;
    }

    public static boolean isValidList(List list) {
        return list != null && list.size() > 0;
    }

    private AppUtils(){
        throw new AssertionError("No Instances");
    }

}
