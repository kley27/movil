package com.tec.cley.project.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tec.cley.project.R;
import com.tec.cley.project.activities.MainActivity;
import com.tec.cley.project.adapters.PatientsAdapter;
import com.tec.cley.project.controllers.PatientsController;
import com.tec.cley.project.databinding.LayoutRecyclerBinding;
import com.tec.cley.project.models.Patient;
import com.tec.cley.project.utils.AppUtils;
import com.tec.cley.project.utils.InjectorUtils;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by miguelchan on 31/03/17.
 */

public class PatientListFragment extends Fragment {

    public static final String TAG = PatientListFragment.class.getSimpleName();

    private static final String EXTRA_DELETION = "ExtraDeletion";

    @Inject
    /*package*/ PatientsController mController;

    private RecyclerView mRecyclerView;
    private LayoutRecyclerBinding mBinding;
    private ItemTouchHelper mHelper;

    private ArrayList<Patient> mPatientLists;
    private boolean mIsDeletion;

    public static PatientListFragment newInstance(boolean isDeletion) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_DELETION, isDeletion);
        PatientListFragment patientListFragment = new PatientListFragment();
        patientListFragment.setArguments(args);

        return patientListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InjectorUtils.getInjector(this).inject(this);
        loadArguments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_recycler, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = LayoutRecyclerBinding.bind(view);
        mRecyclerView = mBinding.recyclerLayoutRecyclerView;
        mayLoadPatients();

        if(mIsDeletion) {
            showHintMessage();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadPatients();
    }

    private void loadArguments() {
        Bundle args = getArguments();
        if(AppUtils.isNotNull(args)) {
            mIsDeletion = args.getBoolean(EXTRA_DELETION, true);
        }
    }

    private void showHintMessage() {
        Toast.makeText(getActivity(), "Deslize su dedo izquierda ó derecha para borrar", Toast.LENGTH_LONG).show();
    }

    private void mayLoadPatients() {
        if(mPatientLists == null) {
            loadPatients();
        }
    }

    private void loadPatients() {
        mPatientLists = AppUtils.newArrayList(mController.getPatients());
        configureAdapterData();
    }

    private void configureAdapterData() {
        PatientsAdapter adapter = new PatientsAdapter(mPatientLists);
        mRecyclerView.setAdapter(adapter);

        if(mIsDeletion && mHelper == null) {
            mHelper = new ItemTouchHelper(mCallback);
            mHelper.attachToRecyclerView(mRecyclerView);
        } else if(!mIsDeletion) {
            adapter.setOnPatientClickListener(this::onPatientClick);
        }
    }

    private void deleteSelectedPatient(int position) {
        Patient patient = mPatientLists.get(position);
        mController.deletePatient(patient);
        loadPatients();
    }

    private void showConfirmDeletionDialog(final int position) {
        AlertDialog alertDialog = new AlertDialog
                .Builder(getActivity())
                .setTitle("Confirmar Borrado")
                .setMessage("¿Desea Borrar al Paciente?")
                .setPositiveButton("Borrar", (dialogInterface, i) -> {
                    deleteSelectedPatient(position);
                })
                .setNegativeButton("Cancelar", null)
                .create();
        alertDialog.show();
    }

    private void onPatientClick(Patient patient, int position, View view) {
        PatientsFragment patientsFragment = PatientsFragment.newInstance(patient);

        if(getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).loadFragment(patientsFragment, PatientsFragment.TAG, true);
        }
    }

    private ItemTouchHelper.SimpleCallback mCallback =
            new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView,
                              RecyclerView.ViewHolder viewHolder,
                              RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int selectedItem = viewHolder.getAdapterPosition();
            showConfirmDeletionDialog(selectedItem);
        }
    };

}
